defmodule JustThisPostWeb.PostController do
  use JustThisPostWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
