defmodule JustThisPostWeb.PostLive do
  use Phoenix.LiveView
  use Phoenix.HTML

  defmodule Post do
    defstruct [:body]
  end

  def mount(_params, _session, socket) do
    {:ok, assign(socket, val: 72, body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.")}
  end

  def handle_event("inc", _, socket) do
    {:noreply, update(socket, :val, &(&1 + 1))}
  end

  def handle_event("body_changed", value, socket) do
    {:noreply, assign(socket, :body, value)}
  end

  def handle_event("dec", _, socket) do
    {:noreply, update(socket, :val, &(&1 - 1))}
  end
end
